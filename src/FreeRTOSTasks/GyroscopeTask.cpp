#include "FreeRTOSTasks/GyroscopeTask.hpp"

void GyroscopeTask::execute() {
    for (uint8_t i = 0; i < NumberOfGyroscopes; i++) {
        gyroscopes[i].getRevisionID();
    }

    while (true) {
        setGyroscopeParameters();
        setMonitoringParameters();
        printData();
        vTaskDelay(pdMS_TO_TICKS(delayMs));
    }
}

void GyroscopeTask::printData() {
    etl::string<150> log = "";
    log += "\n\rrateX=";
    etl::to_string(AcubeSATParameters::adcsGyroscopeRateX.getValue(), log, etl::format_spec().precision(6), true);
    log += " rateY=";
    etl::to_string(AcubeSATParameters::adcsGyroscopeRateY.getValue(), log, etl::format_spec().precision(6), true);
    log += " rateZ=";
    etl::to_string(AcubeSATParameters::adcsGyroscopeRateZ.getValue(), log, etl::format_spec().precision(6), true);
    log += " temperatureX=";
    etl::to_string(AcubeSATParameters::adcsGyroscopeXTemperature.getValue(), log, etl::format_spec().precision(6), true);
    log += " temperatureY=";
    etl::to_string(AcubeSATParameters::adcsGyroscopeYTemperature.getValue(), log, etl::format_spec().precision(6), true);
    log += " temperatureZ=";
    etl::to_string(AcubeSATParameters::adcsGyroscopeZTemperature.getValue(), log, etl::format_spec().precision(6), true);
    log += " HICST X=";
    etl::to_string(AcubeSATParameters::adcsGyroXHICSTRegister.getValue(), log, etl::format_spec().precision(6), true);

    LOG_DEBUG << log.data();
}

void GyroscopeTask::setGyroscopeParameters() {
    constexpr etl::array<AxisParameters, NumberOfGyroscopes> axisParams = {{
        {AcubeSATParameters::adcsGyroscopeRateX, AcubeSATParameters::adcsGyroscopeXTemperature, AcubeSATParameters::adcsGyroXMeasurementUnsuccessful},
        {AcubeSATParameters::adcsGyroscopeRateY, AcubeSATParameters::adcsGyroscopeYTemperature, AcubeSATParameters::adcsGyroYMeasurementUnsuccessful},
        {AcubeSATParameters::adcsGyroscopeRateZ, AcubeSATParameters::adcsGyroscopeZTemperature, AcubeSATParameters::adcsGyroZMeasurementUnsuccessful}
    }};

    for (uint8_t i = 0; i < NumberOfGyroscopes; i++) {
        auto rateResult = gyroscopes[i].getRate();
        auto tempResult = gyroscopes[i].getTemperatureValue();

        updateParameter(rateResult ? etl::optional<double>{rateResult.value()}
                                   : etl::nullopt,
                        axisParams[i].rateParam, axisParams[i].errorParam);

        updateParameter(tempResult ? etl::optional<double>{tempResult.value()}
                                   : etl::nullopt,
                        axisParams[i].tempParam, axisParams[i].errorParam);
    }
}

void GyroscopeTask::setMonitoringParameters() {
    const etl::array<MonitoringTest, numberOfAxis> tests{{
        {"LOCST",
            getLOCST,
            AcubeSATParameters::adcsGyroXLOCSTRegister,
            AcubeSATParameters::adcsGyroYLOCSTRegister,
            AcubeSATParameters::adcsGyroZLOCSTRegister},
        {"HICST",
            getHICST,
            AcubeSATParameters::adcsGyroXHICSTRegister,
            AcubeSATParameters::adcsGyroYHICSTRegister,
            AcubeSATParameters::adcsGyroZHICSTRegister},
        {"QUAD",
            getQUAD,
            AcubeSATParameters::adcsGyroXQUADRegister,
            AcubeSATParameters::adcsGyroYQUADRegister,
            AcubeSATParameters::adcsGyroZQUADRegister}
    }};

    const etl::array<char, numberOfAxis> axes{'X', 'Y', 'Z'};

    for (uint8_t i = 0; i < NumberOfGyroscopes; i++) {
        char axis = axes[i];

        for (const auto& test : tests) {
            auto expectedResult = test.getter(gyroscopes[i]);
            etl::optional<double> result = expectedResult.has_value()
                ? etl::optional<double>{static_cast<double>(expectedResult.value())}
            : etl::nullopt;
            auto& param = (axis == 'X') ? test.xParam :
                         (axis == 'Y') ? test.yParam :
                                       test.zParam;
            auto& errorFlag = (axis == 'X') ? AcubeSATParameters::adcsGyroXMeasurementUnsuccessful :
                            (axis == 'Y') ? AcubeSATParameters::adcsGyroYMeasurementUnsuccessful :
                                          AcubeSATParameters::adcsGyroZMeasurementUnsuccessful;
            updateParameter(result, param, errorFlag);
        }

        auto faultResult = gyroscopes[i].getErrorFlags();
        if (faultResult) {
            processFaultRegister(faultResult.value(), axis);
        } else if (faultResult.error() == ADXRS453::ADXRS453Error::MEASUREMENT_FAILED) {
            auto& errorParam = (axis == 'X') ? AcubeSATParameters::adcsGyroXMeasurementUnsuccessful :
                             (axis == 'Y') ? AcubeSATParameters::adcsGyroYMeasurementUnsuccessful :
                                           AcubeSATParameters::adcsGyroZMeasurementUnsuccessful;
            errorParam.setValue(true);
        } else {
            auto& spiParam = (axis == 'X') ? AcubeSATParameters::adcsGyroXSPIInvalidArg :
                           (axis == 'Y') ? AcubeSATParameters::adcsGyroYSPIInvalidArg :
                                         AcubeSATParameters::adcsGyroZSPIInvalidArg;
            spiParam.setValue(true);
        }
    }
}