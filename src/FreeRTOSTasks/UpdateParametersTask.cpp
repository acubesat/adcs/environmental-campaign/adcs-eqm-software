#include "UpdateParametersTask.hpp"

void UpdateParametersTask::execute() {
    TaskHandle_t reportParametersHandle = xTaskGetHandle(TaskName);

    while (true) {
        AcubeSATParameters::adcsBootCounter.setValue(
                static_cast<uint16_t>(BootCounter::GPBRRead(BootCounter::BootCounterRegister)));
        AcubeSATParameters::adcsSystick.setValue(static_cast<uint64_t>(xTaskGetTickCount()));
        vTaskDelay(pdMS_TO_TICKS(delayMs));
    }
}
