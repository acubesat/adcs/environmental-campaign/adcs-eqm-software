#include "FreeRTOSTasks/MagnetometerTask.hpp"

void MagnetometerTask::execute() {
  magnetometer.getREVID();

  while (true) {
    auto data = magnetometer.readMeasurements();
    if (data != RM3100::RM3100Error::NO_ERROR) {
      LOG_DEBUG << "Magnetometer Error";
    } else {
      setMagnetometerParameters();
      printData();
    }
    selfTestCheck();
    vTaskDelay(pdMS_TO_TICKS(delayMs));
  }
}

void MagnetometerTask::printData() {
  etl::string<80> log = "";
  log += "x=";
  etl::to_string(AcubeSATParameters::adcsMagnetometerRawX.getValue(), log,
                 etl::format_spec().precision(6), true);
  log += " y=";
  etl::to_string(AcubeSATParameters::adcsMagnetometerRawY.getValue(), log,
                 etl::format_spec().precision(6), true);
  log += " z=";
  etl::to_string(AcubeSATParameters::adcsMagnetometerRawZ.getValue(), log,
                 etl::format_spec().precision(6), true);
  log += " magneticField=";
  etl::to_string(magnetometer.getMagneticFieldNorm(), log,
                 etl::format_spec().precision(9), true);

  LOG_DEBUG << log.data();
}

void MagnetometerTask::setMagnetometerParameters() {
  AcubeSATParameters::adcsMagnetometerRawX.setValue(magnetometer.getX());
  AcubeSATParameters::adcsMagnetometerRawY.setValue(magnetometer.getY());
  AcubeSATParameters::adcsMagnetometerRawZ.setValue(magnetometer.getZ());
}

void MagnetometerTask::selfTestCheck() {
  auto BIST = magnetometer.selfTest();
  if (BIST.has_value()) {
    if ((BIST.value() & 0b11000000) != 0b11000000) {
      AcubeSATParameters::adcsMagnetometerZAxisOK.setValue(true);
    }
    if ((BIST.value() & 0b10100000) != 0b10100000) {
      AcubeSATParameters::adcsMagnetometerYAxisOK.setValue(true);
    }
    if ((BIST.value() & 0b10010000) != 0b10010000) {
      AcubeSATParameters::adcsMagnetometerXAxisOK.setValue(true);
    }
  } else if (BIST.error() == RM3100::RM3100Error::MEASUREMENT_FAILED) {
    AcubeSATParameters::adcsMagnetometerMeasurementUnsuccessful.setValue(true);
  } else {
    AcubeSATParameters::adcsMagnetometerSPIInvalidArg.setValue(true);
  }
}
