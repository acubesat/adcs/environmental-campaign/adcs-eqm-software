#include "main.h"
#include "FreeRTOS.h"
#include "list.h"
#include "task.h"
#include "queue.h"
#include "definitions.h"
#include "ADCS_Definitions.hpp"
#include "TaskInitialization.hpp"
#include "UpdateParametersTask.hpp"
#include "MagnetometerTask.hpp"
#include "GyroscopeTask.hpp"

#define IDLE_TASK_SIZE 200

#if configSUPPORT_STATIC_ALLOCATION
/* static memory allocation for the IDLE task */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[IDLE_TASK_SIZE];

extern "C" void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer,
                                              uint32_t *pulIdleTaskStackSize) {
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
    *ppxIdleTaskStackBuffer = &xIdleStack[0];
    *pulIdleTaskStackSize = IDLE_TASK_SIZE;
}

#endif


extern "C" void main_cpp() {

    SYS_Initialize(NULL);

    initializeTasks();
    initializeAmbientTemperatureTask();
    magnetometerTask.emplace();
    gyroscopeTask.emplace();
    updateParametersTask.emplace();

    gyroscopeTask->createTask();
    updateParametersTask->createTask();
    magnetometerTask->createTask();
    resetChecks();

    vTaskStartScheduler();

    while (true) {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks();
    }

    return;
}