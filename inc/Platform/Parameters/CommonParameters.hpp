#pragma once

#include "Helpers/Parameter.hpp"
#include "ADCS_Definitions.hpp"
#include "AcubeSATParameters.hpp"

namespace CommonParameters {
    inline auto &boardTemperature1 = AcubeSATParameters::adcsBoardTemperature1;
    inline auto &boardTemperature2 = AcubeSATParameters::adcsBoardTemperature2;
    inline auto &mcuTemperature = AcubeSATParameters::adcsMCUTemperature;
    inline auto &time = AcubeSATParameters::adcsOnBoardTime;
    inline auto &useRTT = AcubeSATParameters::adcsUseRTT;
    inline auto &useCAN = AcubeSATParameters::adcsUseCAN;
    inline auto &useUART = AcubeSATParameters::adcsUseUART;
}