#ifndef ADCS_SOFTWARE_ADCS_DEFINITIONS_HPP
#define ADCS_SOFTWARE_ADCS_DEFINITIONS_HPP

#include <cstdint>
#include "Logger_Definitions.hpp"
#include "Definitions.hpp"

/**
 * @defgroup Definitions of ADCS_SOFTWARE Defined Constants
 *
 * This file contains constant definitions that are used in the ADCS_SOFTWARE configuration.
 *
 */

/**
 * ADCS uses 3 gyroscopes for X, Y, Z axes
 */
inline const uint8_t NumberOfGyroscopes = 3;

/**
 * Maximum string size for a LogLevel is 9 characters long
 */
inline const uint8_t MaxLogNameSize = 9;

/**
 * TickType_t is a uint32_t number. Its string representation is at most 10 digits long.
 */
inline const uint8_t MaxTickCountStringSize = 10;

/**
 * Maximum size for a string representation of a service or message type identifier
 */
inline const uint8_t MaxTypeIDStringSize = 2;

/**
 * The maximum code number an analog signal can be converted into by the ADC of the AFEC peripheral with 12-bit resolution
 */
inline const uint16_t MaxADCValue = 4095;

/**
 * The positive voltage reference of the MCU
 */
inline const uint16_t PositiveVoltageReference = 3300;

/**
 * The typical slope of voltage versus temperature of the internal temperature sensor
 */
inline const float TemperatureSensitivity = 2.33;

/**
 * The reference temperature of the internal temperature sensor for the voltage-to-temperature conversion
 */
inline const uint8_t ReferenceTemperature = 25;

/**
 * The size of the queue used to communicate with the UART Gatekeeper task
 */
inline const uint8_t UARTQueueSize = 5;


inline const uint8_t MaxUsartTCSize = 128;

inline const uint8_t TCQueueCapacity = 10;
/**
 * The typical voltage output of the DAC of the AFEC channel at 25 Celsius
 */
inline const uint16_t TypicalVoltageAt25 = 720;

/**
 * Set to 1 for Continuous Measurement Mode and 0 for Single Measurement Mode for the RM3100 magnetometer.
 */
inline const uint8_t RM3100ContinuousMeasurementMode = 1;

/**
 * The number of sensor oscillation cycles that will be counted during a measurement sequence, where 200 is the default value.
 */
inline const uint16_t RM3100CycleCount = 200;

inline String<LogSource::MaximumLettersInSubsystemName> LogSource::currentSubsystem = "ADCS";

#endif //ADCS_SOFTWARE_ADCS_DEFINITIONS_HPP
