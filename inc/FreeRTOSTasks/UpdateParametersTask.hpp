#pragma once

#include "Task.hpp"

/**
 * FreeRTOS task for periodically updating specific parameters using ParameterService functionality.
 */
class UpdateParametersTask : public Task {
private:
    static constexpr TickType_t delayMs = 3000;
public:
    static constexpr uint16_t TaskStackDepth = 7000;

    StackType_t taskStack[TaskStackDepth]{};

    void execute();

    UpdateParametersTask() : Task("ParameterUpdating") {}

    /**
     * Create FreeRTOS task
     */
    void createTask() {
        xTaskCreateStatic(vClassTask < UpdateParametersTask > , this->TaskName, UpdateParametersTask::TaskStackDepth,
                          this, tskIDLE_PRIORITY + 1, this->taskStack, &(this->taskBuffer));
    }

};

inline std::optional<UpdateParametersTask> updateParametersTask;