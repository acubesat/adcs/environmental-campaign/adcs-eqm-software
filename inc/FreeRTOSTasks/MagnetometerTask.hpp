#pragma once

#include "RM3100.hpp"
#include "Task.hpp"

/**
 * FreeRTOS Task that collects magnetometer measurements
 */
class MagnetometerTask : public Task {
private:
    static constexpr TickType_t delayMs = 3000;

    RM3100 magnetometer;

    /**
     * Updates the magnetometer parameters
     */
    void setMagnetometerParameters();

public:
    MagnetometerTask()
        : Task("MagnetometerTask"),
          magnetometer(RM3100ContinuousMeasurementMode, RM3100CycleCount,
                       RM3100CycleCount, RM3100CycleCount, MAG_CS_PIN,
                       MAG_DRDY_PIN) {}

    static constexpr uint16_t taskStackDepth = 1500;

    StackType_t taskStack[taskStackDepth]{};

    void execute();

    /**
     * Prints magnetometer measurements
     */
    void printData();

    void selfTestCheck();

    /**
     * Create FreeRTOS task
     */
    void createTask() {
        xTaskCreateStatic(vClassTask<MagnetometerTask>, this->TaskName,
                          MagnetometerTask::taskStackDepth, this,
                          tskIDLE_PRIORITY + 3, this->taskStack,
                          &(this->taskBuffer));
    }
};

inline std::optional<MagnetometerTask> magnetometerTask;