#pragma once

#include "ADXRS453.hpp"
#include "Task.hpp"

class GyroscopeTask : public Task {
private:
  static constexpr TickType_t delayMs = 1000;
  etl::array<ADXRS453, NumberOfGyroscopes> gyroscopes{
      ADXRS453(GYR_CS2_PIN), ADXRS453(GYR_CS3_PIN), ADXRS453(GYR_CS1_PIN)};

  void setGyroscopeParameters();

  void setMonitoringParameters();

public:
  static constexpr uint16_t taskStackDepth = 4000;
  static constexpr uint8_t numberOfAxis = 3;
  static constexpr uint8_t numberOfFaultBits = 8;

  struct AxisParameters {
    decltype(AcubeSATParameters::adcsGyroscopeRateX) &rateParam;
    decltype(AcubeSATParameters::adcsGyroscopeXTemperature) &tempParam;
    decltype(AcubeSATParameters::adcsGyroXMeasurementUnsuccessful) &errorParam;
  };

  using GetterFunction =
      etl::expected<uint16_t, ADXRS453::ADXRS453Error> (*)(ADXRS453 &);
  static etl::expected<uint16_t, ADXRS453::ADXRS453Error>
  getLOCST(ADXRS453 &g) {
    return g.getLowContinuousSelfTestFailure();
  }

  static etl::expected<uint16_t, ADXRS453::ADXRS453Error>
  getHICST(ADXRS453 &g) {
    return g.getHighContinuousSelfTestFailure();
  }

  static etl::expected<uint16_t, ADXRS453::ADXRS453Error> getQUAD(ADXRS453 &g) {
    return g.getQuadratureError();
  }

  struct MonitoringTest {
    const char *name;
    GetterFunction getter;
    decltype(AcubeSATParameters::adcsGyroXLOCSTRegister) &xParam;
    decltype(AcubeSATParameters::adcsGyroYLOCSTRegister) &yParam;
    decltype(AcubeSATParameters::adcsGyroZLOCSTRegister) &zParam;
  };

  struct FaultBitMapping {
    uint16_t mask;
    decltype(AcubeSATParameters::adcsGyroXFAULTRegisterFailBit) &xParam;
    decltype(AcubeSATParameters::adcsGyroYFAULTRegisterFailBit) &yParam;
    decltype(AcubeSATParameters::adcsGyroZFAULTRegisterFailBit) &zParam;
  };

  StackType_t taskStack[taskStackDepth]{};

  GyroscopeTask() : Task("GyroscopeTask"){};

  void execute();

  void printData();

  template <typename T, typename P, typename E>
  static void updateParameter(const etl::optional<T> &value, P &parameter,
                              E &errorFlag) {
    if (value.has_value()) {
      parameter.setValue(value.value());
    } else {
      errorFlag.setValue(true);
    }
  }

  void processFaultRegister(uint16_t faultValue, char axis) {
    const etl::array<FaultBitMapping, numberOfFaultBits> faultBits{
        {{0b0000100000000000, AcubeSATParameters::adcsGyroXFAULTRegisterFailBit,
          AcubeSATParameters::adcsGyroYFAULTRegisterFailBit,
          AcubeSATParameters::adcsGyroZFAULTRegisterFailBit},
         {0b0000001000000000, AcubeSATParameters::adcsGyroXFAULTRegisterOVBit,
          AcubeSATParameters::adcsGyroYFAULTRegisterOVBit,
          AcubeSATParameters::adcsGyroZFAULTRegisterOVBit},
         {0b0000000100000000, AcubeSATParameters::adcsGyroXFAULTRegisterUVBit,
          AcubeSATParameters::adcsGyroYFAULTRegisterUVBit,
          AcubeSATParameters::adcsGyroZFAULTRegisterUVBit},
         {0b0000000010000000, AcubeSATParameters::adcsGyroXFAULTRegisterPPLBit,
          AcubeSATParameters::adcsGyroYFAULTRegisterPPLBit,
          AcubeSATParameters::adcsGyroZFAULTRegisterPPLBit},
         {0b0000000001000000, AcubeSATParameters::adcsGyroXFAULTRegisterQbit,
          AcubeSATParameters::adcsGyroYFAULTRegisterQbit,
          AcubeSATParameters::adcsGyroZFAULTRegisterQbit},
         {0b0000000000100000, AcubeSATParameters::adcsGyroXFAULTRegisterNVMBit,
          AcubeSATParameters::adcsGyroYFAULTRegisterNVMBit,
          AcubeSATParameters::adcsGyroZFAULTRegisterNVMBit},
         {0b0000000000010000, AcubeSATParameters::adcsGyroXFAULTRegisterPORBit,
          AcubeSATParameters::adcsGyroYFAULTRegisterPORBit,
          AcubeSATParameters::adcsGyroZFAULTRegisterPORBit},
         {0b0000000000000100, AcubeSATParameters::adcsGyroXFAULTRegisterCSTBit,
          AcubeSATParameters::adcsGyroYFAULTRegisterCSTBit,
          AcubeSATParameters::adcsGyroZFAULTRegisterCSTBit}}};

    for (const auto &faultBit : faultBits) {
      if (faultValue & faultBit.mask) {
        switch (axis) {
        case 'X':
          faultBit.xParam.setValue(true);
          break;
        case 'Y':
          faultBit.yParam.setValue(true);
          break;
        case 'Z':
          faultBit.zParam.setValue(true);
          break;
        default:
          LOG_DEBUG << "WRONG AXIS PASSED AS AN ARGUMENT";
          break;
        }
      }
    }
  }

  /**
   * Create FreeRTOS task
   */
  void createTask() {
    xTaskCreateStatic(vClassTask<GyroscopeTask>, this->TaskName,
                      GyroscopeTask::TaskStackDepth, this, tskIDLE_PRIORITY + 3,
                      this->taskStack, &(this->taskBuffer));
  }
};

inline std::optional<GyroscopeTask> gyroscopeTask;
